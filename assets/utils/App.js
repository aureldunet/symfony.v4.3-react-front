import React, { Component } from "react";
import Example from "../components/Example";

class App extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return <div className="container">
            <h1>{this.props.sources.title}</h1>
            <Example examples={this.props.sources.examples}/>
        </div>;
    }

}

export default App;
