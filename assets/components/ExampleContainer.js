import React, { Component } from "react";

class ExampleContainer extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return <li id={this.props.item.id}>{this.props.item.name}</li>;
    }

}

export default ExampleContainer;
