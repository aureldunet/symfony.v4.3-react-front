import React, {Component} from "react";
import ExampleContainer from "./ExampleContainer";

class Example extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return <ul>
            {this.props.examples.map(function (item, i) {
                return <ExampleContainer key={i} item={item}/>
            })}
        </ul>;
    }

}

export default Example;
