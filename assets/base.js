import React from 'react';
import ReactDom from 'react-dom';
import App from './utils/App';

import './styles.scss';

ReactDom.render(<App sources={sources}/>, document.getElementById('app'));
