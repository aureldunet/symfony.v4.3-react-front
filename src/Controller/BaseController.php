<?php

namespace App\Controller;

use App\Repository\ExampleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{

    /**
     * @Route("/", name="base")
     *
     * @param ExampleRepository $repository
     * @return Response
     */
    public function index(ExampleRepository $repository): Response
    {
        return $this->render('base.html.twig', [
            'sources' => [
                'title'     => 'welcome',
                'examples'  => $repository->findAllToArray(),
            ]
        ]);
    }

}
