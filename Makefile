include .env

DOCKER_COMPOSE = docker-compose
DOCKER_COMPOSE_EXEC = $(DOCKER_COMPOSE) exec
DOCKER_COMPOSE_RUN = $(DOCKER_COMPOSE) run
SERVICE_APP = app
USER_WWW-DATA = -u www-data
EXEC_APP = $(DOCKER_COMPOSE_EXEC) $(SERVICE_APP)
EXEC_APP_USER = $(DOCKER_COMPOSE_EXEC) $(USER_WWW-DATA) $(SERVICE_APP)
EXEC_APP_COMPOSER = $(EXEC_APP_USER) composer
EXEC_APP_CONSOLE = $(EXEC_APP_USER) bin/console
EXEC_APP_YARN = $(EXEC_APP_USER) yarn

up:
	$(DOCKER_COMPOSE) -f $(APP_COMPOSE_FILE) up -d --build

down:
	$(DOCKER_COMPOSE) down -v

composer-install:
	$(EXEC_APP_COMPOSER) install --no-suggest --no-progress

db-drop:
	$(EXEC_APP_CONSOLE) doctrine:database:drop --force --if-exists

db-create:
	$(EXEC_APP_CONSOLE) doctrine:database:create --if-not-exists

db-migrate:
	$(EXEC_APP_CONSOLE) doctrine:migrations:migrate -n

db-diff:
	$(EXEC_APP_CONSOLE) doctrine:migrations:diff

db-fixtures:
	$(EXEC_APP_CONSOLE) doctrine:fixtures:load -n

db-install: db-drop db-create db-migrate

permission-install:
	$(EXEC_APP) chmod $(APP_MODE) -R .

yarn-install:
	$(EXEC_APP_YARN) install

yarn-build:
	$(EXEC_APP_YARN) encore $(APP_ENV)

cache-clear:
	$(EXEC_APP_CONSOLE) cache:clear --env=$(APP_ENV)

install: up composer-install db-install yarn-install yarn-build cache-clear permission-install
